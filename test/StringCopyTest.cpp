//
// Created by bareya on 25/11/18.
//

// This example is to check for temporaries, I use HeapTrack

#include <GolaemCache/GolaemCache.h>

#include <iostream>

#include <climits>
#include <glm_crowd_io.h>

using namespace Golaem;

//struct Potato
//{
//    void peel()
//    {
//    }
//};

struct Potato
{
    void peel();
};

inline void Potato::peel()
{
}


void createGeometryGenerationContext()
{
    // must be initialized first, otherwise glmBeginGeometryGeneration segfaults
    glmInitCrowdIO();

    int inputFrame = 1050;

    GlmGeometryGenerationContext* _geometryContext;

    _geometryContext = static_cast<GlmGeometryGenerationContext*>(malloc(sizeof(GlmGeometryGenerationContext)));
    memset(_geometryContext, 0, sizeof(GlmGeometryGenerationContext));
    _geometryContext->_frame = inputFrame;

    _geometryContext->_crowdFieldCount = 1;
    _geometryContext->_crowdFieldNames = (char(*)[GIO_NAME_LENGTH])malloc(
        _geometryContext->_crowdFieldCount * GIO_NAME_LENGTH * sizeof(char));
    strcpy(_geometryContext->_crowdFieldNames[0], "crowdField1");

    strcpy(_geometryContext->_cacheName, "rnd_crowds_tests_golaemManLight_v0001");

    _geometryContext->_cacheDirectoryCount = 1;
    _geometryContext->_cacheDirectories = (char(*)[GIO_NAME_LENGTH])malloc(
        _geometryContext->_cacheDirectoryCount * GIO_NAME_LENGTH * sizeof(char));
    strcpy(_geometryContext->_cacheDirectories[0], "/mnt/linux/cpp/HoudiniGolaem/test/data/");

    _geometryContext->_characterFilesDirectoryCount = 1;
    _geometryContext->_characterFilesDirectories = (char(*)[GIO_NAME_LENGTH])malloc(
        _geometryContext->_characterFilesDirectoryCount * GIO_NAME_LENGTH * sizeof(char));
    strcpy(_geometryContext->_characterFilesDirectories[0], "");

    _geometryContext->_excludedEntityCount = 0;
    _geometryContext->_renderPercent = 1.f;
    strcpy(_geometryContext->_terrainFile, "");
    strcpy(_geometryContext->_layoutFile, "");
    _geometryContext->_enableLayout = 1;
    _geometryContext->_maxVerticesPerFace = UINT_MAX; // no max limit to face edge count, can use 4 to limit to quads, or 3 to limit to triangles.
    _geometryContext->_computeEdgeVisibility = 0;     // or 1 to fill _triangleEdgeVisibility, see glm_crowd_io.h

    using GeoGenCtxDeleter = std::function<void(GlmGeometryGenerationContext*)>;
    using GeoGenCtxPtr = std::unique_ptr<GlmGeometryGenerationContext, GeoGenCtxDeleter>;
    GeoGenCtxPtr geometryContext{_geometryContext, [](GlmGeometryGenerationContext* ptr) {
                                     free(ptr->_crowdFieldNames);
                                     free(ptr->_cacheDirectories);
                                     free(ptr->_characterFilesDirectories);
                                     free(ptr->_excludedEntities);
                                 }};

    auto status = glmBeginGeometryGeneration(geometryContext.get());
    std::cout << glmConvertGeometryGenerationStatus(status) << std::endl;
}

int main()
{
    //    CacheParameters parms;
    //    parms.getSimulationPath() = GOLAEM_FILE_PATH_SIMULATION;
    //
    //    exint frame = 2;
    //
    //    UT_Array<Entity> entities = GolaemCache::getEntities(parms, EntityLOD::Skeleton, frame);
    //    for(auto it= entities.begin(); it!=entities.end(); ++it)
    //    {
    //        Frame* frameCache = GolaemCache::getFrame(*it);
    //        std::cout << frameCache->getData()->_bonePositions[0][0] << std::endl;
    //        //std::cout << it->m_id << " " << it->m_frameNumber << " " << it->getIndex() << std::endl;
    //    }

    //    auto terrain1 = CrowdTerrain::loadTerrainAsset("/mnt/linux/cpp/HoudiniGolaem/test/data/rnd_crowds_tests_golaemManLight_v0001.crowdField1.terrain.gtg");
    //    auto terrain2 = CrowdTerrain::loadTerrainAsset("/mnt/linux/cpp/HoudiniGolaem/test/data/rnd_crowds_tests_golaemManLight_v0001.crowdField1.terrain.gtg");
    //
    //    std::cout << terrain1->_subMeshes[0] << std::endl;
    //    std::cout << terrain2->_subMeshes[0] << std::endl;
    //
    ////    GlmEntityGeometry* geo;
    //
    //    std::map<int, std::weak_ptr<int>> values;
    //    {
    //        auto int1 = std::make_shared<int>(1012);
    //        values.emplace(10, int1);
    //
    //        auto found = values.find(10);
    //        if(!found->second.expired())
    //        {
    //            std::cout << "First: "  << *(found->second.lock()) << std::endl;
    //        }
    //    }
    //
    //    {
    //        auto found = values.find(10);
    //        if(found->second.expired())
    //        {
    //            values.erase(10);
    //            auto int1 = std::make_shared<int>(2012);
    //            values.emplace(10, int1);
    //
    //            auto found_created = values.find(10);
    //            std::cout << "Second created: "  << *(found_created->second.lock()) << std::endl;
    //        }
    //        else
    //        {
    //            std::cout << "Second not expired: "  << *(found->second.lock()) << std::endl;
    //        }
    //    }

    //createGeometryGenerationContext();

    return 0;
}