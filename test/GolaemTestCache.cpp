//
// Created by bareya on 30/11/18.
//

#include <gtest/gtest.h>

#include <GolaemCache/Geometry.h>
#include <GolaemCache/GolaemCache.h>
#include <GolaemCache/Simulation.h>

#include "GolaemTestPath.h"

#include <glm_crowd_io.h>

using namespace Golaem;

class CacheParametersTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        // Simple simulation, only simulation file
        simpleParms.setSimulationPath(GOLAEM_TEST_SIMULATION_PATH);
        simpleParms.appendCharacter(GOLAEM_TEST_CHAR_GCHA);

        // Modified simulation, contains layout
        modifiedParms.setSimulationPath(GOLAEM_TEST_SIMULATION_PATH);
        modifiedParms.setLayoutPath(GOLAEM_TEST_LAYOUT_PATH);
        modifiedParms.setTerrainDestination(GOLAEM_TEST_TERRAIN_DESTINATION_PATH);
        modifiedParms.appendCharacter(GOLAEM_TEST_CHAR_GCHA);

        // fake Simple Simulation
        fakeSimpleParms.setSimulationPath("non_existing_simulation_path");

        // fake Modified Simulation
        fakeModifiedParms.setSimulationPath("non_existing_simulation_path");
        fakeModifiedParms.setLayoutPath("non_existing_layout_path");
        fakeModifiedParms.setTerrainDestination("non_existing_terrain_path");
    }

    CacheParameters simpleParms;
    CacheParameters modifiedParms;

    CacheParameters fakeSimpleParms;
    CacheParameters fakeModifiedParms;

    long frameNumber{1050};
};

//
//
//
TEST_F(CacheParametersTest, Attributes)
{
    // Simple parms
    ASSERT_EQ(simpleParms.getSimulationPath(), GOLAEM_TEST_SIMULATION_PATH);
    ASSERT_EQ(simpleParms.getDirectory(), GOLAEM_TEST_CACHE_DIR);

    std::string cacheName, fieldName;
    simpleParms.getSimulationComponents(cacheName, fieldName);
    ASSERT_EQ(cacheName, GOLAEM_TEST_CACHE_NAME);
    ASSERT_EQ(fieldName, GOLAEM_TEST_FIELD_NAME);

    ASSERT_EQ(simpleParms.getTerrainSource(), GOLAEM_TEST_TERRAIN_SOURCE_PATH);

    // fake simple parms
    EXPECT_THROW(fakeSimpleParms.getSimulationComponents(cacheName, fieldName), std::exception);
}

//
// Test to open terrain and test no-throw option
//
TEST_F(CacheParametersTest, Terrain)
{
    // when cache exists, in both cases it should not throw
    EXPECT_NO_THROW(GolaemCache::getTerrain(modifiedParms.getTerrainDestination().c_str(), true));
    EXPECT_NO_THROW(GolaemCache::getTerrain(modifiedParms.getTerrainDestination().c_str(), false));

    // query non existing cache
    EXPECT_THROW(GolaemCache::getTerrain("non_existing_cache", true /* throw */), std::exception);
    EXPECT_NO_THROW(GolaemCache::getTerrain("non_existing_cache", false /* no throw */));

    // should return nullptr if cache does not exist
    ASSERT_EQ(GolaemCache::getTerrain("non_existing_cache", false), nullptr);

    // multiple queries should return same object
    auto terrainA = GolaemCache::getTerrain(modifiedParms.getTerrainDestination().c_str());
    auto terrainB = GolaemCache::getTerrain(modifiedParms.getTerrainDestination().c_str());
    ASSERT_EQ(terrainA, terrainB);
}

TEST_F(CacheParametersTest, Layout)
{
    auto simulation = GolaemCache::getInstance().getSimulation(modifiedParms);
    ASSERT_EQ(simulation->isModified(), true);

    auto frame = simulation->getFrame(1050);
    ASSERT_EQ(frame->isModified(), true);

    std::cout << simulation->getData()->_entityCount << std::endl;

    std::vector<Entity> entities;
    GolaemCache::getEntities(entities, modifiedParms, EntityLOD::Skeleton, 1050);
    std::cout << entities.size() << std::endl;
}

//
// Constructors for Simulation, Frame, Layout, must throw.
//
TEST_F(CacheParametersTest, ThrowingConstructor)
{
    const GolaemCache& golaemCache = GolaemCache::getInstance();

    EXPECT_THROW(golaemCache.getTerrain("non_existing_terrain"), std::exception);

    EXPECT_THROW(golaemCache.getSimulation(fakeSimpleParms), std::exception);
    EXPECT_NO_THROW(golaemCache.getSimulation(simpleParms));

    std::shared_ptr<Simulation> simCache = golaemCache.getSimulation(simpleParms);
    EXPECT_THROW(simCache->getFrame(-1), std::exception);
}

//
// Frame holds a shared pointer to Simulation.
// Simulation can not expire before Frame.
// This test checks for proper weak pointer cache expiry.
//
TEST_F(CacheParametersTest, SimpleFrameExpiry)
{
    std::weak_ptr<Simulation> simCacheWeak;
    {
        std::shared_ptr<Frame> frameCacheShared;

        {
            std::shared_ptr<Simulation> simCache = GolaemCache::getInstance().getSimulation(simpleParms);
            simCacheWeak = simCache;

            frameCacheShared = simCache->getFrame(frameNumber);

            // pointers must be equal.
            ASSERT_EQ(simCache.get(), frameCacheShared->getSimulationCache());
        }

        // Simulation can not expire because
        // Frame holds shared instance of the simulation.
        ASSERT_EQ(simCacheWeak.expired(), false);
    }

    // Frame instance goes out of scope, Simulation goes too.
    ASSERT_EQ(simCacheWeak.expired(), true);
}

TEST_F(CacheParametersTest, SimulationLayout)
{
    //GolaemCache::getInstance().getSimulation(simpleParms);
}

TEST_F(CacheParametersTest, GeometryGenerationContext)
{
    EXPECT_NO_THROW(GeoContext::create(simpleParms, frameNumber));

    std::shared_ptr<GeoContext> contextCache = GeoContext::create(simpleParms, frameNumber);
    ASSERT_NE(contextCache->getData(), nullptr);

    std::vector<Entity> entities;
    GolaemCache::getEntities(entities, simpleParms, EntityLOD::SkinDisplay, frameNumber);
    for (Entity& e : entities)
    {

        std::cout << e.getId() << " " << e.getGeometryData()->_meshCount << std::endl;
    }
}