//
// Created by bareya on 26/11/18.
//

#ifndef HOUDINIGOLAEM_GU_PACKEDGOLAEM_H
#define HOUDINIGOLAEM_GU_PACKEDGOLAEM_H

#include <GU/GU_PackedImpl.h>
#include <GU/GU_PrimPacked.h>

#include <GolaemCache/GolaemCache.h>

class GU_PackedGolaem : public GU_PackedImpl
{
public:
    GU_PackedGolaem()
        : GU_PackedImpl()
        , m_entityParams{}
    {
    }

    GU_PackedGolaem(const GU_PackedGolaem& other)
        : GU_PackedImpl(other)
    {
        m_entityParams = Golaem::Entity{other.m_entityParams};
    }

    static void install(GA_PrimitiveFactory* f);

    GU_PackedFactory *getFactory() const override;

    GU_PackedImpl *copy() const override;

    bool isValid() const override;

    void clearData() override;

    bool load(GU_PrimPacked *prim, const UT_Options &options, const GA_LoadMap &map) override;

    void update(GU_PrimPacked *prim, const UT_Options &options) override;

    bool save(UT_Options &options, const GA_SaveMap &map) const override;

    bool getBounds(UT_BoundingBox &box) const override;

    bool getRenderingBounds(UT_BoundingBox &box) const override;

    void getVelocityRange(UT_Vector3 &min, UT_Vector3 &max) const override;

    void getWidthRange(fpreal &wmin, fpreal &wmax) const override;

    bool unpack(GU_Detail &destgdp, const UT_Matrix4D *transform) const override;

    int64 getMemoryUsage(bool inclusive) const override;

    void countMemory(UT_MemoryCounter &counter, bool inclusive) const override;

    static const GA_PrimitiveTypeId &typeId()
    {
        return theTypeId;
    }

    // Golaem stuff
    static GU_PrimPacked* build(GU_Detail& detail, Golaem::Entity parms);

    const Golaem::Entity& getEntityParameters() const
    {
        return m_entityParams;
    }

private:
    static GA_PrimitiveTypeId theTypeId;

    Golaem::Entity m_entityParams;
};

#endif //HOUDINIGOLAEM_GU_PACKEDGOLAEM_H
