//
// Created by bareya on 26/11/18.
//

#include "GU_PackedGolaem.h"
#include "GT_GEOPackedGolaemCollect.h"

#include <GolaemCache/Simulation.h>

#include <GU/GU_PackedFactory.h>
#include <GU/GU_PrimPacked.h>
#include <FS/UT_DSO.h>

#include <iostream>


namespace
{

UT_String thePackedName{"PackedGolaem"};
UT_String thePackedLabel{"Packed Golaem"};

class GolaemPackedFactory : public GU_PackedFactory
{
public:
    GolaemPackedFactory()
        : GU_PackedFactory(thePackedName, thePackedLabel)
    {
    }

    GU_PackedImpl* create() const override
    {
        return new GU_PackedGolaem();
    }
};

GolaemPackedFactory* theFactory = nullptr;

} // namespace


GA_PrimitiveTypeId GU_PackedGolaem::theTypeId(-1);

GU_PackedFactory* GU_PackedGolaem::getFactory() const
{
    return theFactory;
}

GU_PackedImpl* GU_PackedGolaem::copy() const
{
    return new GU_PackedGolaem(*this);
}

bool GU_PackedGolaem::isValid() const
{
    // TODO this has to be removed

//    try
//    {
//        auto frameCache = Golaem::GolaemCache::getFrame(*(getEntityParameters().m_parms), getEntityParameters().m_frameNumber);
//        auto frameData = frameCache->getData();
//    }
//    catch(const std::exception& e)
//    {
//        return false;
//    }

    return true;
}

void GU_PackedGolaem::clearData()
{
}

bool GU_PackedGolaem::load(GU_PrimPacked* prim, const UT_Options& options, const GA_LoadMap& map)
{
    return false;
}

void GU_PackedGolaem::update(GU_PrimPacked* prim, const UT_Options& options)
{

std::cout << "Update" << std::endl;
}

bool GU_PackedGolaem::save(UT_Options& options, const GA_SaveMap& map) const
{
    return false;
}

bool GU_PackedGolaem::getBounds(UT_BoundingBox& box) const
{
    return false;
}

bool GU_PackedGolaem::getRenderingBounds(UT_BoundingBox& box) const
{
    return false;
}

void GU_PackedGolaem::getVelocityRange(UT_Vector3& min, UT_Vector3& max) const
{
}

void GU_PackedGolaem::getWidthRange(fpreal& wmin, fpreal& wmax) const
{
}

bool GU_PackedGolaem::unpack(GU_Detail& destgdp, const UT_Matrix4D* transform) const
{
    return false;
}

int64 GU_PackedGolaem::getMemoryUsage(bool inclusive) const
{
    return 0;
}

void GU_PackedGolaem::countMemory(UT_MemoryCounter& counter, bool inclusive) const
{
}

void GU_PackedGolaem::install(GA_PrimitiveFactory* gaFactory)
{
    if (theFactory)
    {
        return;
    }

    theFactory = new GolaemPackedFactory();
    GU_PrimPacked::registerPacked(gaFactory, theFactory);

    if(theFactory->isRegistered())
    {
        // get run time type id from the factory, and bind it with gt collector
        theTypeId = theFactory->typeDef().getId();
        GT_GEOPackedGolaemCollect::registerPrimitive(theTypeId);
    }
    else
    {
        fprintf(stderr, "Unable to register packed sphere from %s\n", UT_DSO::getRunningFile());
    }
}


GU_PrimPacked *GU_PackedGolaem::build(GU_Detail &detail, Golaem::Entity parms) {
    auto packedPrim = GU_PrimPacked::build(detail, thePackedName);
    auto impl = UTverify_cast<GU_PackedGolaem*>(packedPrim->implementation());
    impl->m_entityParams = std::move(parms);
    return packedPrim;
}
