//
// Created by bareya on 25/11/18.
//

#ifndef HOUDINIGOLAEM_SOP_GOLAEMIMPORTER_H
#define HOUDINIGOLAEM_SOP_GOLAEMIMPORTER_H

#include <SOP/SOP_Node.h>

class SOP_GolaemImporter : public SOP_Node
{
public:
    static PRM_Template* buildTemplates();
    static OP_Node* create(OP_Network* net, const char* name, OP_Operator* op);

    static const UT_StringHolder theSOPTypeName;

private:
    SOP_GolaemImporter(OP_Network* net, const char* name, OP_Operator* op);

    OP_ERROR cookMySop(OP_Context& context) override;
};

#endif //HOUDINIGOLAEM_SOP_GOLAEMIMPORTER_H
