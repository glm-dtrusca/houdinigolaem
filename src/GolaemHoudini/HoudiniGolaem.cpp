//
// Created by bareya on 25/11/18.
//

#include <UT/UT_DSOVersion.h>
#include <OP/OP_OperatorTable.h>
#include <GU/GU_Detail.h>

#include "SOP_GolaemImporter.h"
#include "GU_PackedGolaem.h"

// Here we register all known types

// register new sop
void newSopOperator(OP_OperatorTable *table)
{
    table->addOperator(new OP_Operator(
            SOP_GolaemImporter::theSOPTypeName,
            "Golaem Importer",
            SOP_GolaemImporter::create,
            SOP_GolaemImporter::buildTemplates(),
            0,
            0,
            nullptr,
            OP_FLAG_GENERATOR));
}

// register new primitive
void newGeometryPrim(GA_PrimitiveFactory* f)
{
    GU_PackedGolaem::install(f);
}