//
// Created by bareya on 29/11/18.
//

#include "Geometry.h"
#include "CacheParameters.h"

#define GLMC_IMPLEMENTATION
#include <glm_crowd_io.h>

using namespace Golaem;

GeoContext::GeoContext(const CacheParameters& parms, long frame)
{
    GlmGeometryGenerationContext* _geometryContext;
    _geometryContext = static_cast<GlmGeometryGenerationContext*>(GLMC_MALLOC(sizeof(GlmGeometryGenerationContext)));
    memset(_geometryContext, 0, sizeof(GlmGeometryGenerationContext));

    auto directory = parms.getDirectory(); // TODO cache it

    // cache directory
    _geometryContext->_cacheDirectoryCount = 1;
    _geometryContext->_cacheDirectories = (char(*)[GIO_NAME_LENGTH])GLMC_MALLOC(_geometryContext->_cacheDirectoryCount * GIO_NAME_LENGTH * sizeof(char));
    strcpy(_geometryContext->_cacheDirectories[0], directory.c_str());

    std::string cacheName, fieldName;
    parms.getSimulationComponents(cacheName, fieldName);

    // cache name
    strcpy(_geometryContext->_cacheName, cacheName.c_str());

    // field name
    _geometryContext->_crowdFieldCount = 1;
    _geometryContext->_crowdFieldNames = (char(*)[GIO_NAME_LENGTH])GLMC_MALLOC(_geometryContext->_crowdFieldCount * GIO_NAME_LENGTH * sizeof(char));
    strcpy(_geometryContext->_crowdFieldNames[0], fieldName.c_str());

    // character files
    const auto& characters = parms.getCharacters();
    _geometryContext->_characterFilesDirectoryCount = static_cast<uint8_t>(characters.size());
    _geometryContext->_characterFilesDirectories = (char(*)[GIO_NAME_LENGTH])GLMC_MALLOC(_geometryContext->_characterFilesDirectoryCount * GIO_NAME_LENGTH * sizeof(char));
    for (unsigned long i{}; i < characters.size(); ++i)
    {
        strcpy(_geometryContext->_characterFilesDirectories[i], characters[i].c_str());
    }

    // layout
    _geometryContext->_enableLayout = !parms.getLayoutPath().empty();
    strcpy(_geometryContext->_layoutFile, parms.getLayoutPath().c_str());

    // terrain file
    strcpy(_geometryContext->_terrainFile, "");

    // frame
    _geometryContext->_frame = frame;

    // extra settings
    _geometryContext->_excludedEntityCount = 0;
    _geometryContext->_renderPercent = 1.f;
    _geometryContext->_maxVerticesPerFace = UINT_MAX; // no max limit to face edge count, can use 4 to limit to quads, or 3 to limit to triangles.
    _geometryContext->_computeEdgeVisibility = 0;     // or 1 to fill _triangleEdgeVisibility, see glm_crowd_io.h
    _geometryContext->_instancingEnabled = 0;
    _geometryContext->_motionBlurEnabled = 0;

    // wrap into shared pointer for auto clear
    GeoGenCtxPtr geometryContext{_geometryContext, [](GlmGeometryGenerationContext* ctx) {
                                     glmEndGeometryGeneration(ctx);

                                     GLMC_FREE(ctx->_crowdFieldNames);
                                     GLMC_FREE(ctx->_cacheDirectories);
                                     GLMC_FREE(ctx->_characterFilesDirectories);
                                     GLMC_FREE(ctx->_excludedEntities);

                                     GLMC_FREE(ctx);
                                 }};

    auto status = glmBeginGeometryGeneration(_geometryContext);
    if (status != GIO_SUCCESS)
    {
        throw std::runtime_error("Failed to begin geometry generation constext");
    }

    // set attributes
    m_parms = parms;
    m_context = std::move(geometryContext);
}

GeoContext::GeoContext(const CacheParametersHandle& parms, long frame)
{
}

const GlmEntityGeometry* Geometry::getData()
{
    auto contextData = m_context->getData();

    if (!m_geometry)
    {
        GlmEntityGeometry* geo = (GlmEntityGeometry*)GLMC_MALLOC(sizeof(GlmEntityGeometry));
        GlmEntityInstanceMatrices instanceMatrices;
        GlmEntityBoundingBox* bbox = &(contextData->_entityBBoxes[m_index]);
        auto geoStatus = glmCreateEntityGeometry(geo, contextData, &(contextData->_entityBBoxes[m_index]), instanceMatrices);
        m_geometry = GeometryPtr{geo, [contextData, bbox](GlmEntityGeometry* geo) {
                                     glmDestroyEntityGeometry(geo, contextData, bbox);

                                     GLMC_FREE(geo);
                                 }};

        if (geoStatus != GIO_SUCCESS)
        {
            throw std::runtime_error("Can not generate geometry for given entity.");
        }

        return geo;
    }

    return m_geometry.get();
}

Geometry::Geometry(const GeoContextHandle& context, long index)
    : m_context{context}
    , m_index{index}
{
}
