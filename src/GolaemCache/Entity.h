//
// Created by bareya on 02/12/18.
//

#ifndef GOLAEMCACHE_ENTITY_H
#define GOLAEMCACHE_ENTITY_H

#include "CacheParameters.h"
#include "Handles.h"

namespace Golaem
{

enum class EntityLOD
{
    Box,
    Skeleton,
    SkinDisplay,
//    SkinRender
};

/// Aggregates all entries needed to create simulation
class Entity
{
public:
    Entity() = default;

    Entity(const CacheParameters& parms,
           const FrameHandle& frameCache,
           EntityLOD lod,
           long index);

    /// EntityCache create for geometry generation
    Entity(const CacheParameters& parms,
           const GeometryHandle& geoGenContext,
           EntityLOD lod,
           long index);

    explicit Entity(const Entity&) = default;
    Entity(Entity&&) = default;

    ~Entity();

    Entity& operator=(const Entity&) = delete;
    Entity& operator=(Entity&&) = default;

    // Update, if necessary, and return Simulation and Frame data
    const GlmSimulationData* getSimulationData() const;
    const GlmFrameData* getFrameData() const;
    const GlmEntityGeometry* getGeometryData() const;

    EntityLOD lod() const noexcept
    {
        return m_lod;
    }

    long getIndex() const noexcept
    {
        return m_index;
    }

    long getId() const noexcept
    {
        return m_id;
    }

private:
    void updateSimulationFrameCache() const
    {
    }

    void updateGeometryGenerationCache() const
    {
    }

    CacheParameters m_newParms;

    EntityLOD m_lod;
    long m_id;
    long m_index;
    long m_frameNumber;

    // Frame data
    mutable FrameHandle m_frame;
    mutable GeometryHandle m_geometry;

    mutable bool m_dirty{true};
};

} // namespace Golaem

#endif // GOLAEMCACHE_ENTITY_H
