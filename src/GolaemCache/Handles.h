//
// Created by bareya on 03/12/18.
//

#ifndef GOLAEMCACHE_HANDLES_H
#define GOLAEMCACHE_HANDLES_H

#include <functional>
#include <memory>

struct GlmSimulationData_v0;
typedef GlmSimulationData_v0 GlmSimulationData;

struct GlmFrameData_v0;
typedef GlmFrameData_v0 GlmFrameData;

struct GlmHistory_v0;
typedef GlmHistory_v0 GlmHistory;

struct GlmEntityTransform_v0;
typedef GlmEntityTransform_v0 GlmEntityTransform;

struct GlmGeometryGenerationContext_0;
typedef GlmGeometryGenerationContext_0 GlmGeometryGenerationContext;

struct GlmEntityGeometry_0;
typedef GlmEntityGeometry_0 GlmEntityGeometry;

struct GlmEntityBoundingBox_0;
typedef GlmEntityBoundingBox_0 GlmEntityBoundingBox;

namespace CrowdTerrain
{
struct Mesh;
}

namespace Golaem
{

class CacheParameters;
using CacheParametersHandle = std::shared_ptr<CacheParameters>;

class Simulation;
using SimulationHandle = std::shared_ptr<Simulation>;

class Frame;
using FrameHandle = std::shared_ptr<Frame>;

class GeoContext;
using GeoContextHandle = std::shared_ptr<GeoContext>;

class Geometry;
using GeometryHandle = std::shared_ptr<Geometry>;

using TerrainHandle = std::shared_ptr<CrowdTerrain::Mesh>;

} // namespace Golaem

#endif //GOLAEMCACHE_HANDLES_H
